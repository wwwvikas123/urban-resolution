package main

import (
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/prometheus/procfs"
)

var (
	ProcFS, _ = procfs.NewDefaultFS()
	memFree = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Name: "mem_free",
			Help: "Free memory",
		},
	)
	procCount = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Name: "proc_count",
			Help: "Total count of running processes",
		},
	)
)

func init() {
	prometheus.MustRegister(memFree)
	prometheus.MustRegister(procCount)
}

func main() {
	go collectMemInfo()
	go collectProcStat()
        port := ":8080"

	http.Handle("/metrics", promhttp.Handler())
        fmt.Printf("listening on port %s\n", port) 
	err := http.ListenAndServe(port, nil)
	if err != nil {
		fmt.Printf("Failed to start HTTP server: %s\n", err.Error())
		os.Exit(1)
	}
}

func collectMemInfo() {
	for {
		memStat, err := ProcFS.Meminfo()
		if err != nil {
			fmt.Printf("Failed to retrieve memory stats: %s\n", err.Error())
			continue
		}
		memFree.Set(float64(*memStat.MemFree))

		time.Sleep(10 * time.Second)
	}
}

func collectProcStat() {
	for {
		processes, err := ProcFS.AllProcs()
		if err != nil {
			fmt.Printf("Failed to retrieve running processes: %s\n", err.Error())
			continue
		}
		procCount.Set(float64(len(processes)))

		time.Sleep(10 * time.Second)
	}
}
