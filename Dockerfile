FROM golang:1.21 AS build
WORKDIR /app
COPY . .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o app main.go

FROM gcr.io/distroless/base-debian10
WORKDIR /app
COPY --from=build /app/app .
EXPOSE 8080
CMD ["./app"]
